//
//  ViewController.swift
//  ios84ls3
//
//  Created by WA on 02.02.2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        reverseNumber(number: 10)
//
//        _ = doStuff()
//        numbers()
//        numbers()
//        stringExample()
        arrayExample()
//        dictionaryExample()
    }

    func doStuff() -> String {
        print("STTTTT")
        return "STUFF"
    }

    // task 0
    func printBiggerNumber(numberOne: Int, numberTwo: Int) {
//        let math = constantValue
        if numberOne == numberTwo {
            print("\(numberOne) is EQUAL to \(numberTwo)")
        } else if numberOne > numberTwo {
            print("\(numberOne) is bigger than \(numberTwo)")
        } else {
            print("\(numberTwo) is bigger than \(numberOne)")
        }
    }

    // task 1
    func math(number: Int) {
        let sq = number * number
        let qube = sq * number
        print("Square of number \(number) is \(sq) qube is \(qube)")
    }

    // task 2
    func reverseNumber(number: Int) {
//        var counter = 0
//        for _ in 0...number {
//            let reverseValue = number - counter
//            print(counter, reverseValue)
//            counter = counter + 1
//        }
    
        for value in 0...number {
            print(value, number - value)
        }
    }

    func numbers() {
        print(Int.random(in: 0...10))
        
    }

    func stringExample() {
        for symbol in "_______&@#(%)!#)$(!@*" {
            print(symbol)
        }

        let lecturerName = "Artem Ar"
        print(lecturerName.count)
        print(lecturerName.replacingOccurrences(of: "Ar", with: "Nitro"))
        
        if let firstCharacter = lecturerName.first {
            print(firstCharacter)
        }

        let emptyString = ""
        if let firstCharacter = emptyString.first {
            print(firstCharacter)
        } else {
            print("VALUE IS NIL")
        }

        
    }

    func arrayExample() {
//        let array: Array<Int> = [1]
        let arrayInts = [5, 1, 8, 2, 20, 3, 4, 1, 20]
        let set = Set(arrayInts)
        print("SET \(set)")
        let arrayString = ["Artem", "Maxim", "Dmitriy"]
        var emptyArray = [String]()
        emptyArray = arrayString
        print(emptyArray)

        for name in emptyArray {
            print("Hello, \(name)")
        }

        emptyArray.append("Elena")

        let index = 3
        if emptyArray.count < index {
            print("ARRAY DOESN'T CONTAIN ELEMENT WITH index \(index)")
        } else {
            print(emptyArray[index])
        }

//        arrayInts
        // REMOVE LAST ELEMENT OF ARRAY
//        emptyArray.removeLast()
        let elena = emptyArray.first { element -> Bool in
            return element == "Elena"
        }
        if elena == "Elena" {
            print("ELENA WAS IN ARRAY")
        }
        print(elena)

        emptyArray.insert("Ihor", at: 0)

        let filteredNames = emptyArray.filter { element -> Bool in
            return element.count == 5
        }

        var sortedInts = arrayInts.sorted { (first, next) -> Bool in
            return first > next
        }
        
        print(sortedInts)
        print(filteredNames)

        sortedInts.append(contentsOf: [2, 5, 10])
        print(sortedInts)

        

        // MAX/MIN VAlue in array
//        sortedInts.max() // .min()

//        emptyArray.remove(at: <#T##Int#>)
    }

    func dictionaryExample() {
        var dict: [String: [Int]] = [
            "Police": [100000],
            "Emergency": [200000],
            "My": [666, 6667]
        ]

        print(dict["Police"])
        dict["Humanity"] = []
        print(dict["Humanity"])
        print(dict)

        dict.removeValue(forKey: "Police")
        print(dict.keys.count)
        print(dict.values.count)

//        for i in dict {
//            let key = i.key
//            let value = i.value
//        }
    
        let alphabet = [
            "a": "A",
            "b": "B"
        ]
    }
}

